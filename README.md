Woodhaus is a small casual place where you can come and hang out with your friends. Well be featuring small craft beer producers and small batch winemakers. The food is centered around the wood fire oven, where well be doing Neapolitan style pizza with some salads and sides, and roasting local vegetables.

Website: https://wood-haus.com/